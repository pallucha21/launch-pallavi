import * as React from 'react';
import {
  AlertGroup,
  AlertVariant,
  Alert,
  Button,
  PageSection,
  Split,
  SplitItem,
  Title,
  Breadcrumb,
  BreadcrumbItem,
  Modal,
  Form,
  FormGroup,
  FormHelperText,
  FormAlert,
  TextInput,
  TextArea,
  ActionGroup,
  ModalVariant,
  FormSelect,
  FormSelectOption,
  HelperTextItem,
  HelperText,
  FlexItem,
  ButtonVariant,
  Flex,
  TextContent,
  Tooltip,
  Text
} from '@patternfly/react-core';
import { Link } from 'react-router-dom';
import { sortable, headerCol, cellWidth } from '@patternfly/react-table';
import { ActionList } from '@app/lib/ActionList';
import { AuthContext } from '@app/lib/AuthProvider';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import {
  CreateOrganizationRequest,
  UpdateOrganizationRequest,
  GetOrganizationsResponse,
  Member,
  member_RoleToJSON,
  member_StateToJSON,
  userStateToJSON,
  accessModeToJSON,
  AccessMode,
  RequestOrganizationMembershipRequest,
  MembershipType,
  DeleteOrganizationRequest,
  GetEntityTypeConfigurationsResponse,
} from '@mergetb/api/portal/v1/workspace_types';
import { useFetch } from 'use-http';
import { OrganizationCard } from './OrganizationCard';
import { BallotOutlined, GridViewOutlined } from '@mui/icons-material';
import { styled } from '@mui/material/styles';
import { InfoCircleIcon } from '@patternfly/react-icons';

const StyledTooltip = styled(Tooltip)({
  '& .pf-v5-c-tooltip__content': {
    backgroundColor: 'white',
    color: 'black',
    boxShadow: '0 2px 4px rgba(0,0,0,0.1)',
    border: '1px solid #d2d2d2',
    padding: '10px',
    maxWidth: '500px', 
    width: '400px',     
    fontSize: '14px',
    lineHeight: '1.4',
    maxHeight: '300px', 
  },
  '& .pf-v5-c-tooltip__arrow': {
    display: 'none', 
  },
});


const StyledBallotOutlined = styled(BallotOutlined)(({ theme }) => ({
  fontSize: '24px', 
  color: 'inherit',
  marginRight: '8px',
  verticalAlign: 'middle', 
}));

const StyledGridViewOutlined = styled(GridViewOutlined)(({ theme }) => ({
  fontSize: '24px', 
  color: 'inherit',
  marginRight: '8px',
  verticalAlign: 'middle', 
}));

const Organizations: React.FunctionComponent = () => {
  const { api } = React.useContext(GeneralSettingsContext);
  const { identity } = React.useContext(AuthContext);
  const username = identity?.traits.username;
  const [searchTerm, setSearchTerm] = React.useState('');
  const [reloadTrigger, setReloadTrigger] = React.useState(0);

  const userview_last = (localStorage.getItem('userview') ?? true) == true;
  const [viewLabel, setViewLabel] = React.useState('View ' + (userview_last ? 'All' : 'Own'));
  const [userView, setUserView] = React.useState(userview_last);
  const [alerts, setAlerts] = React.useState([]);
  const [reload, setReload] = React.useState(1);

  const [showNew, setShowNew] = React.useState(false);
  const [newOrgName, setNewOrgName] = React.useState('');
  const [newOrgNameValid, setNewOrgNameValid] = React.useState(false);
  const [newOrgDesc, setNewOrgDesc] = React.useState('');
  const [invalidName, setInvalidName] = React.useState('');

  const [orgCat, setOrgCat] = React.useState('');
  const [orgSubCat, setOrgSubCat] = React.useState('');
  const [orgSubCats, setOrgSubCats] = React.useState(Array<string>);
  const [otherSubCat, setOtherSubCat] = React.useState('');

  const [isCardView, setIsCardView] = React.useState(false);
  const [noResults, setNoResults] = React.useState(false);

  const handleViewChange = (newView: 'table' | 'card') => {
    setIsCardView(newView === 'card');
  };

  const handleSearchResults = (hasResults: boolean) => {
    setNoResults(!hasResults);
  };

  const options: RequestInit & { cachePolicy?: string } = {
    credentials: 'include',
    cachePolicy: 'no-cache',
  };

  const { data: entTypeData } = useFetch(api + '/configurations/entitytype', options, []);
  
  const columns = [
    { title: 'Name', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(15)] },
    { title: 'Description', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(20)] },
    { title: 'State', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(10)] },
    { title: 'Access Mode', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(20)] },
    { title: 'Members', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(15)] },
    { title: 'Projects', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(15)] },
    { title: 'Category', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(15)] },
    { title: 'Subcategory', cellTransforms: [headerCol()], transforms: [sortable, cellWidth(15)] },
  ];

  

  const entityTypes = React.useMemo(() => {
    if (entTypeData) {
      if (Object.prototype.hasOwnProperty.call(entTypeData, 'Types')) {
        const types = GetEntityTypeConfigurationsResponse.fromJSON(entTypeData).Types;
        // Force first category choice in GUI
        if (types.length > 0) {
          setOrgCat(types[0].etype);
          return types;
        }
      }
    }
    return undefined;
  }, [entTypeData]);
  
  React.useEffect(() => {
    console.log("orgCat updated:", orgCat);
  }, [orgCat]);

  React.useEffect(() => {
    console.log("orgSubCat updated:", orgSubCat);
  }, [orgSubCat]);

  const setMode = (oid, mode) => {
    fetch(api + '/organization/' + oid, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        accessMode: {
          value: mode,
        },
      }),
    })
      .then((resp) => {
        if (resp.ok) {
          addAlert('Organization ' + oid + ' access mode set to ' + accessModeToJSON(mode), 'success');
          setReload(reload + 1);
        }
        if (resp.status !== 200) {
          addAlert('Error: ' + resp.statusText, 'danger');
        }
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };

  const deleteOrg = (org: string) => {
    fetch(api + '/organization/' + org, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user: username,
        name: org,
      }),
    })
      .then((resp) => {
        if (resp.ok) {
          addAlert('Organization ' + org + ' Deleted', 'success');
          setReload(reload + 1);
        }
        if (resp.status !== 200) {
          addAlert('Error: ' + resp.statusText, 'danger');
        }
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };

  const reqMembership: (oid: string, uid: string) => void = (oid, uid) => {
    fetch(api + '/organization/' + oid + '/member/' + uid, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        organization: oid,
        id: uid,
        kind: MembershipType.UserMember,
      }),
    })
      .then((resp) => {
        if (resp.ok) {
          addAlert('Membership in ' + oid + ' requested for ' + uid, 'success');
          setReload(reload + 1);
        }
        if (resp.status !== 200) {
          addAlert('Error: ' + resp.statusText, 'danger');
        }
      })
      .catch((error) => {
        console.log(error);
        addAlert(error.message, 'danger');
      });
  };
  
const actions = [
  {
    title: 'Request User Membership',
    onClick: (_event, _rowId, rowData) => {
      const oid = rowData.name.props.text;
      reqMembership(oid, username);
    },
  },
  {
    isSeparator: true,
  },
  {
    title: 'Set Access Public',
    onClick: (_event, _rowId, rowData) => {
      const oid = rowData.name.props.text;
      setMode(oid, AccessMode.Public);
    },
  },
  {
    title: 'Set Access Protected',
    onClick: (_event, _rowId, rowData) => {
      const oid = rowData.name.props.text;
      setMode(oid, AccessMode.Protected);
    },
  },
  {
    title: 'Set Access Private',
    onClick: (_event, _rowId, rowData) => {
      const oid = rowData.name.props.text;
      setMode(oid, AccessMode.Private);
    },
  },
  {
    isSeparator: true,
  },
  {
    title: 'Delete',
    onClick: (_event, _rowId, rowData) => {
      const orgName = rowData.name.props.text;
      if (window.confirm(`Are you sure you want to delete the organization "${orgName}"?`)) {
        deleteOrg(orgName);
      }
    },
  },
];

const mapper = (json) => {
  const orgs = GetOrganizationsResponse.fromJSON(json);
  return orgs.organizations.map((x) => ({
    name: {
      title: <Link to={`/organization/${x.name}`}>{x.name}</Link>,
      props: { text: x.name }
    },
    description: x.description,
    state: userStateToJSON(x.state),
    access_mode: accessModeToJSON(x.accessMode),
    members: {
      title: (
        <>
          {Object.keys(x.members).map((member, index) => (
            <React.Fragment key={member}>
              <Link to={`/user/${member}`}>{member}</Link>
              {index < Object.keys(x.members).length - 1 ? ', ' : ''}
            </React.Fragment>
          ))}
        </>
      ),
      props: { text: Object.keys(x.members).join(', ') }
    },
    projects: {
      title: (
        <>
          {Object.keys(x.projects).map((project, index) => (
            <React.Fragment key={project}>
              <Link to={`/project/${project}`}>{project}</Link>
              {index < Object.keys(x.projects).length - 1 ? ', ' : ''}
            </React.Fragment>
          ))}
        </>
      ),
      props: { text: Object.keys(x.projects).join(', ') }
    },
    category: x.category,
    subcategory: x.subcategory,
  }));
};

  const onOrgNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const val = event.target.value;
    const re = /^[a-z][a-z0-9]*$/;
    if (re.test(val) === false) {
      setNewOrgName(val);
      setNewOrgNameValid(false);
      setInvalidName(
        'The organization name must start with a lowercase letter and only contain lowercase alphanumeric characters and no spaces'
      );
    } else {
      setInvalidName('');
      setNewOrgName(val);
      setNewOrgNameValid(val !== '');
    }
  };

  const toggleNewOrg = () => {
    setShowNew(!showNew);
  };

  const toggleView = () => {
    setViewLabel('View ' + (userView === false ? 'All' : 'Own'));
    localStorage.setItem('userview', userView ? '0' : '1');
    setUserView(!userView);
    setReload(reload + 1);
  };

  const addAlert = (t, v) => {
    setAlerts((prev) => [...prev, { title: t, variant: v }]);
  };

  const crumbs = (
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem>Organizations</BreadcrumbItem>
      </Breadcrumb>
    </PageSection>
  );

  const header = (
    <PageSection>
      <Split>
        <SplitItem>
          <Flex alignItems={{ default: 'alignItemsCenter' }} style={{ gap: '4px' }}>
            <FlexItem>
              <Title headingLevel="h1" size="2xl">
                Organizations
              </Title>
            </FlexItem>
            <FlexItem>
              <StyledTooltip
                content={
                  <TextContent>
                    <Text>
                      Organizations are used to manage a (typically large) group of users. When users join an organization, they delegate the authority to manage their account to the creators and maintainers of the organization. New users on the Portal can request membership in an organization when creating the account; when that happens, the organization's maintainers can authorize the account without requiring approval of a Merge Portal administrator. Projects can also join organizations, which gives the organization's maintainers access to the experiments created in that project.
                    </Text>
                  </TextContent>
                }
                position="right"
                enableFlip={false}
                distance={15}
              >
                <Button variant="plain" aria-label="More info for Organizations" style={{ padding: 0 }}>
                  <InfoCircleIcon />
                </Button>
              </StyledTooltip>
            </FlexItem>
          </Flex>
        </SplitItem>
        <SplitItem isFilled />
        <SplitItem>
          <Flex>
            <FlexItem>
              <Button variant="control" aria-label={viewLabel} onClick={toggleView}>
                {viewLabel}
              </Button>
            </FlexItem>
            <FlexItem>
              <Button variant="control" aria-label="New Organization" onClick={toggleNewOrg}>
                New Organization
              </Button>
            </FlexItem>
          </Flex>
        </SplitItem>
      </Split>
    </PageSection>
  );

  const submitForm = () => {
    console.log('Submitting form with:', { newOrgName, newOrgDesc, orgCat, orgSubCat, otherSubCat });
  
    if (notready) {
      console.log('Form is not ready to submit');
      return;
    }
  
    const req: CreateOrganizationRequest = {
      user: username,
      organization: {
        name: newOrgName,
        description: newOrgDesc,
        category: orgCat,
        subcategory: orgSubCat === 'Other' ? otherSubCat : orgSubCat,
        // Remove the oid field
        members: {}, // Empty object for new organization
        projects: {}, // Empty object for new organization
        state: 1, // Use a valid state value, assuming 1 is for active/created state
        accessMode: AccessMode.Private, // Add this line to set a default access mode
      },
    };
  
    console.log('Request body:', JSON.stringify(req, null, 2));

    fetch(api + '/organization/' + newOrgName, {
      method: 'PUT',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(req),
    })
      .then(async (resp) => {
        console.log('Response status:', resp.status);
        const responseText = await resp.text();
        console.log('Response body:', responseText);
        
        if (resp.ok) {
          addAlert('New Organization ' + newOrgName + ' Created', 'success');
          setReload(reload + 1);
          setNewOrgDesc('');
          setNewOrgName('');
          setNewOrgNameValid(false);
          toggleNewOrg();
        } else {
          throw new Error(responseText);
        }
      })
      .catch((error) => {
        console.log('Fetch error:', error);
        addAlert(error.message, 'danger');
      });
  };

  const handleOrgCatChange = (value) => {
    console.log("Category changed to:", value);
    setOrgCat(value);
    setOrgSubCat(''); // Reset subcategory when changing category
    if (entityTypes) {
      const found = entityTypes.find((o) => o.etype === value);
      if (found && found.subtypes) {
        console.log("Subtypes found:", found.subtypes);
        setOrgSubCats(found.subtypes);
        if (found.subtypes.length > 0) {
          setOrgSubCat(found.subtypes[0]); // Set first subcategory as default
        }
      } else {
        console.log("No subtypes found");
        setOrgSubCats([]); // Clear subcategories if none found
      }
    }
  };

  const handleApiResponse = async (response, successMessage) => {
    const responseText = await response.text();
    console.log('Response status:', response.status);
    console.log('Response body:', responseText);
  
    if (response.ok) {
      addAlert(successMessage, 'success');
      setReload(reload + 1);
    } else {
      let errorMessage = `Error: ${response.statusText}`;
      try {
        const errorData = JSON.parse(responseText);
        errorMessage = `Error: ${errorData.message || response.statusText}`;
        console.error('Detailed error:', errorData);
      } catch (e) {
        console.error('Error parsing response:', e);
      }
      addAlert(errorMessage, 'danger');
      throw new Error(errorMessage);
    }
  };

  const notready = React.useMemo(() => {
    console.log("Checking form readiness:", {
      newOrgName,
      newOrgNameValid,
      newOrgDesc,
      orgCat,
      orgSubCats,
      orgSubCat,
      otherSubCat
    });
    return (
      newOrgName === '' ||
      !newOrgNameValid ||
      newOrgDesc === '' ||
      orgCat === '' ||
      (orgSubCats.length > 0 && orgSubCat === '') ||
      (orgSubCat === 'Other' && otherSubCat === '')
    );
  }, [newOrgName, newOrgNameValid, newOrgDesc, orgCat, orgSubCats, orgSubCat, otherSubCat]);

  const newModal = (
    <Modal isOpen={showNew} onClose={toggleNewOrg} variant={ModalVariant.medium} aria-label="New Organization">
      <React.Fragment>
        <Title headingLevel="h1" size="2xl">
          New Organization
        </Title>
        <Form>
          {notready && (
            <FormAlert>
              <Alert variant="danger" title="All fields must be filled" aria-live="polite" isInline />
            </FormAlert>
          )}
         <FormGroup label="Name" fieldId="name">
  <TextInput
    type="text"
    id="name"
    value={newOrgName}
    onChange={onOrgNameChange}
    validated={newOrgNameValid ? 'success' : 'error'}
  />
  {invalidName && (
    <HelperText>
      <HelperTextItem variant="error">{invalidName}</HelperTextItem>
    </HelperText>
  )}
</FormGroup>
<FormGroup label="Description">
  <TextArea
    type="text"
    id="description"
    value={newOrgDesc}
    autoResize={true}
    onChange={(event) => setNewOrgDesc(event.target.value)}
  />
</FormGroup>
<FormGroup label="Category">
  <FormSelect 
    value={orgCat} 
    onChange={(event, value) => handleOrgCatChange(value)} 
    aria-label="Category"
  >
    <FormSelectOption key="placeholder" value="" label="Select a category" isDisabled />
    {entityTypes && entityTypes.map((e, i) => (
      <FormSelectOption key={i} value={e.etype} label={e.etype} />
    ))}
  </FormSelect>
</FormGroup>
{orgSubCats.length !== 0 && (
  <FormGroup label="Subcategory">
    <FormSelect 
      value={orgSubCat} 
      onChange={(event, value) => {
        console.log("Subcategory changed to:", value);
        setOrgSubCat(value);
      }} 
      aria-label="Subcategory"
    >
      <FormSelectOption key="placeholder" value="" label="Select a subcategory" isDisabled />
      {orgSubCats.map((e, i) => (
        <FormSelectOption key={i} value={e} label={e} />
      ))}
    </FormSelect>
  </FormGroup>
)}

          {orgSubCat === 'Other' && (
            <React.Fragment>
              <FormGroup label="Other Subcategory">
                <TextInput
                  isRequired
                  type="text"
                  id="othersubcat"
                  value={otherSubCat}
                  onChange={(e) => setOtherSubCat(e)}
                />
              </FormGroup>
            </React.Fragment>
          )}
          <ActionGroup>
            <Button variant="control" onClick={submitForm} isDisabled={notready} isAriaDisabled={notready}>
              Submit
            </Button>
          </ActionGroup>
        </Form>
      </React.Fragment>
    </Modal>
  );

  const notifications = (
    <AlertGroup isToast>
      {alerts.map((a, i) => (
        <Alert variant={AlertVariant[a.var]} title={a.title} timeout={3000} key={i} />
      ))}
    </AlertGroup>
  );

  let url = api + '/organization';
  if (userView === false) {
    url += '?filter=ByAll';
  }

  return (
    <React.Fragment>
      {alerts.length !== 0 && notifications}
      {newModal}
      {crumbs}
      {header}
      <PageSection>
        <Flex alignItems={{ default: 'alignItemsCenter' }} spaceItems={{ default: 'spaceItemsMd' }}>
          <FlexItem>
            <Button
              variant={isCardView ? ButtonVariant.control : ButtonVariant.primary}
              onClick={() => handleViewChange('table')}
              aria-label="Table view"
              style={{ display: 'inline-flex', alignItems: 'center', justifyContent: 'center' }}
            >
              <StyledBallotOutlined />
              <span style={{ lineHeight: 1 }}>Table</span>
            </Button>
          </FlexItem>
          <FlexItem>
            <Button
              variant={isCardView ? ButtonVariant.primary : ButtonVariant.control}
              onClick={() => handleViewChange('card')}
              aria-label="Card view"
              style={{ display: 'inline-flex', alignItems: 'center', justifyContent: 'center' }}
            >
              <StyledGridViewOutlined />
              <span style={{ lineHeight: 1 }}>Card</span>
            </Button>
          </FlexItem>
        </Flex>
        <div style={{ overflowX: 'auto', marginTop: '16px' }}>
      
        <ActionList
  kind="Organizations"
  columns={columns}
  url={url}
  actions={actions}
  mapper={mapper}
  reload={reload}
  reloadTrigger={reloadTrigger}
  searchTerm={searchTerm}
  setSearchTerm={setSearchTerm}
  onSearchResults={handleSearchResults}
  isCardView={isCardView}
  CardComponent={OrganizationCard}
/>
        </div>
        {noResults && (
          <Alert variant="info" title="No organizations found" isInline>
            No organizations match your search criteria. Try adjusting your search.
          </Alert>
        )}
      </PageSection>
    </React.Fragment>
  );
};


export { Organizations };
