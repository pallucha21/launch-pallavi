import * as React from 'react';
import { Spinner, EmptyState, EmptyStateIcon, EmptyStateHeader } from '@patternfly/react-core';

const AppLoading: React.FunctionComponent = () => {
  // TODO make this a nice page or something.

  return (
    <EmptyState>
      <EmptyStateHeader titleText="Loading Merge Launch" icon={<EmptyStateIcon  icon={Spinner} />} headingLevel="h4" />
    </EmptyState>
  );
};

export { AppLoading };
