import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';

const Facilities: React.FunctionComponent = () => (
  <PageSection>
    <Title headingLevel="h1" size="lg">
      Facility Management
    </Title>
  </PageSection>
);

export { Facilities };
