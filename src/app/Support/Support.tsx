import * as React from 'react';
import { CubesIcon } from '@patternfly/react-icons';
import {
  PageSection,
  Button,
  EmptyState,
  EmptyStateVariant,
  EmptyStateIcon,
  EmptyStateBody,
  EmptyStateActions, EmptyStateHeader, EmptyStateFooter,
} from '@patternfly/react-core';

export interface ISupportProps {
  sampleProp?: string;
}

const Support: React.FunctionComponent<ISupportProps> = () => (
  <PageSection>
    <EmptyState variant={EmptyStateVariant.full}>
      <EmptyStateHeader titleText="MergeTB Support" icon={<EmptyStateIcon icon={CubesIcon} />} headingLevel="h1" />
      <EmptyStateBody>The primary means of Merge support is through the chat and gitlab issues.</EmptyStateBody><EmptyStateFooter>
      <EmptyStateActions>
        <Button component="a" href="https://mergetb.org/docs/experimentation" target="blank" variant="link">
          Documentation
        </Button>
        <Button component="a" href="https://chat.mergetb.net/mergetb" target="blank" variant="link">
          Chat
        </Button>
        <Button component="a" href="https://gitlab.com/mergetb" target="blank" variant="link">
          Source Code
        </Button>
        <Button component="a" href="mailto:contact-project+mergetb-support-email@incoming.gitlab.com" target="blank" variant="link">
          Email Support
        </Button>
        <Button component="a" href="https://gitlab.com/mergetb/support/-/issues" target="blank" variant="link">
          MergeTB Issues (requires gitlab.com account)
        </Button>
      </EmptyStateActions>
    </EmptyStateFooter></EmptyState>
  </PageSection>
);

export { Support };
