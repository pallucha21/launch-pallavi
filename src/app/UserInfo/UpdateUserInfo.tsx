import * as React from 'react';

import { UserInfo } from '@app/UserInfo/UserInfo';

const UpdateUserInfo: React.FunctionComponent = () => {
  return <UserInfo existingUser={true} />;
};

export { UpdateUserInfo };
